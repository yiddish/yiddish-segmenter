Simple rule-based sentence segmenter and tokeniser for Yiddish.

Example:
```shell script
python main.py --input_file=./data/griner-akvarium.txt --output_file=./temp/grinver-akvarium.tok
```

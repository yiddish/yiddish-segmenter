# -*- coding: utf-8 -*-
import argparse
import re
import os
from pathlib import Path

def get_argparser():
  parser = argparse.ArgumentParser()
  parser.add_argument("--input_file", type=str, default='./data/input.txt',
                      help="path to input file")
  parser.add_argument("--output_file", type=str, default='./data/output.txt',
                      help="path to output file")
  
  return parser

alphabets= "(\\w)"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "(\\w[.]\\w[.](?:\\w[.])?)"
websites = "[.](com|net|org|io|gov)"

def split_into_sentences(text):
  text = " " + text + "  "
  text = text.replace("\n","<stop>")
  text = re.sub(prefixes,"\\1<prd>",text)
  text = re.sub(websites,"<prd>\\1",text)
  if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
  text = re.sub("\s" + alphabets + "[.] "," \\1<prd> ",text)
  text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
  text = re.sub(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
  text = re.sub(alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>",text)
  text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
  text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
  text = re.sub(" " + alphabets + "[.]"," \\1<prd>",text)
  if "”" in text: text = text.replace(".”","”.")
  if "\"" in text: text = text.replace(".\"","\".")
  if "!" in text: text = text.replace("!\"","\"!")
  if "?" in text: text = text.replace("?\"","\"?")
  text = text.replace(".",".<stop>")
  text = text.replace("?","?<stop>")
  text = text.replace("!","!<stop>")
  text = text.replace("…","…<stop>")
  text = text.replace("<prd>",".")
  sentences = text.split("<stop>")
  sentences = sentences[:-1]
  sentences = [s.strip() for s in sentences]
  return sentences

def main():
  opts = get_argparser().parse_args()

  Path(opts.output_file).parent.mkdir(parents=True, exist_ok=True)

  replacements = {
    '-': '־',
    "‛": "'",
    "’": "'",
    '“': '"',
    '„': '"',
    '...': '…',
  }

  regexReplacements = {
    r'וי(?!=ִִִִ)': 'ױ',
    r'יי(?!=ִ)': 'ײ',
    r'וו(?!=ּ)': 'װ',
  }

  splits = {
    "אינעם": ["אין", "דעם"],
    "אױפֿן": ["אױף", "דעם"],
    "ביזן": ["ביז", "דעם"],
    "בײַם": ["בײַ", "דעם"],
    "דורכן": ["דורך", "דעם"],
    "מיטן": ["מיט", "דעם"],
    "נאָכן": ["נאָך", "דעם"],
    "ניטאָ": ["ניט", "דאָ"],
    "נישטאָ": ["נישט", "דאָ"],
    "פֿאַרן": ["פֿאַר", "דעם"],
    "פֿונעם": ["פֿון", "דעם"],
    "צום": ["צו", "דעם"],
  }

  verbalPrefixes = [
    "אַדורכ",
    "דורכ",
    "אַהינ",
    "אַהער",
    "אװעק",
    "מיט",
    "אַנטקעגנ",
    "אַקעגנ",
    "אַנידער",
    "אַראָפּ",
    "אַרױס",
    "אַרױפֿ",
    "אַרומ",
    "אַרײַנ",
    "אַרונטער",
    "אַריבער",
    "נאָכ",
    "פֿאַרבײַ"
    "אַהײמ",
    "אַפֿיר",
    "פֿאַרױס",
    "פֿונאַנדער",
    "צוזאַמענ",
    "צונױפֿ",
    "צוריק",
    "אױס",
    "אױפֿ",
    "אומ",
    "אונטער",
    "איבער",
    "אײַנ",
    "אָנ",
    "אָפּ",
    "בײַ",
    "פֿאָר",
    "צו"
  ]

  tsuTemplate = re.compile(r"(" + '|'.join(verbalPrefixes) + ")(צו)(?!=(קן|קט|גן))")
  
  with open(opts.output_file, "w") as output:
    with open(opts.input_file) as input:
      i = 0
      for line in input:
        i = i+1
        for key, value in replacements.items():
          line = line.replace(key, value)
        for key, value in regexReplacements.items():
          line = re.sub(key, value, line)

        sentences = split_into_sentences(line)
        j = 0
        for sentence in sentences:
          if (sentence):
            j = j+1
            sentence = sentence.strip()
            print(f'sentence: {sentence}')
            output.write('\n')
            output.write(f'#File: {os.path.basename(opts.input_file)}\n')
            output.write(f'#Line: {i}, Sentence: {j}\n')
            output.write(f'#Sentence: {sentence}\n')
            words = sentence.split()
            for word in words:
              print(f'initial: {word}')
              prefixes = []
              # separate punctuation in the beginning of the word
              while re.fullmatch('^\\W.+', word):
                prefixes.append(word[:1])
                word = word[1:]
              
              if re.fullmatch("^[סרמכ]'.*", word):
                prefixes.append(word[:2])
                word = word[2:]

              suffixes = []
              # Separate punctuation at the end of the word
              while re.fullmatch('.+(?![ֲִַָֹּ])\\W$', word):
                suffixes.append(word[-1:])
                word = word[:-1]

              # Replace verbs like "vilstu" with "vilst du"
              if re.fullmatch('.+טו', word):
                suffixes.append('דו')
                word = word[:-1]

              # Replace verbs like "mir'n" with "mir veln"
              if re.fullmatch(".+'ן", word):
                suffixes.append('װעלן')
                word = word[:-2]

              for key, value in splits.items():
                if (word==key):
                  for split in value:
                    prefixes.append(split)
                  word = ""
                  break

              tsuMatch = tsuTemplate.match(word)
              if (tsuMatch):
                prefixes.append(word[:tsuMatch.start(2)])
                prefixes.append(word[tsuMatch.start(2):tsuMatch.end(2)])
                prefixes.append(word[tsuMatch.end(2):])
                word = ""
              
              for prefix in prefixes:
                print(f'prefix: {prefix}')
                if len(prefix) > 0:
                  output.write(f'{prefix}\n')

              if len(word) > 0:
                print(f'word: {word}')
                output.write(f'{word}\n')
              
              for suffix in suffixes[::-1]:
                print(f'suffix: {suffix}')
                if len(suffix) > 0:
                  output.write(f'{suffix}\n')

if __name__ == '__main__':
  main()
